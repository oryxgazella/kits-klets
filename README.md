Kits-klets -- Sê jou sê
=================================

Kits-klets (Instant chat) tries to fulfill the following requirement:

Build a multi-user web-based (HTML5) group chat interface with only one text- based chat room
(similar to a single IRC room). Persist up to 100 of the last messages in local storage in the
browser - no server-side persistence required. Messages must be delivered reliably between client
and server, as well as between server and clients. No pre-existing chat libraries or protocols
(e.g. XMPP) or others’ code may be used. JavaScript frameworks such as Bootstrap/jQuery/Angular.js
are allowed.

Approach

* Web sockets
* Angular JS
* Twitter Bootstrap

References

* [Reactive Web Applications with Play](http://www.manning.com/bernhardt/)
* [Play Developer documentation, WebSocketsl](https://www.playframework.com/documentation/2.3.x/ScalaWebSockets)
