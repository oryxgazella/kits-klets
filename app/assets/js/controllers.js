
(function () {
"use strict";

function chatLocation() {
    return "ws://" + location.host + "/chat";
}

var console = window.console;
define(function () {
        var controllers = {};
        controllers.MainCtrl = function ($scope) {
            $scope.connect = function () {
                $scope.connecting = true;
                $scope.webSocket = $scope.webSocket || new WebSocket(chatLocation());
                console.log($scope.name);
                $scope.webSocket.onopen = function () {
                    $scope.webSocket.send(JSON.stringify({
                        "identity": $scope.name,
                        "message": "connected"
                    }));
                };

                $scope.webSocket.onmessage = function (event) {
                    $scope.connecting = false;
                    $scope.connected = true;
                    console.log(event);
                    $scope.chatLog = $scope.chatLog || JSON.parse(localStorage.getItem("chatLog")) || [];
                    var chatLine = JSON.parse(event.data);
                    chatLine.date = new Date();
                    $scope.chatLog.push(chatLine);
                    $scope.chatLog = $scope.chatLog.slice(-100);
                    localStorage.setItem("chatLog", JSON.stringify($scope.chatLog));
                    $scope.$apply();
                };

                $scope.webSocket.onclose = function (event) {
                    $scope.connecting = false;
                    $scope.connected = false;
                    $scope.$apply();
                    console.log("Closing connection to websocket " + event);
                };
            };

            $scope.say = function () {
                $scope.lastMessageSent = $scope.message;
                $scope.webSocket.send(JSON.stringify({
                    "identity": $scope.name,
                    "message": $scope.message
                }));
                $scope.message = "";
            };

            $scope.clearChatLog = function() {
                $scope.chatLog = [];
                localStorage.setItem("chatLog", JSON.stringify([]));
            };
        };
        controllers.MainCtrl.$inject = ['$scope'];
        return controllers;
    }
);
}());
