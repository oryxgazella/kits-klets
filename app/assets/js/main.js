(function () {
    'use strict';

    requirejs.config({
        paths: {
            'angular': ['../lib/angularjs/angular'],
            'angular-route': ['../lib/angularjs/angular-route'],
            'jquery': ['../lib/jquery/jquery'],
            'bootstrap': ['../lib/bootstrap/js/bootstrap']
        },
        shim: {
            'angular': {
                exports: 'angular'
            },
            'angular-route': {
                deps: ['angular'],
                exports: 'angular'
            },
            "bootstrap": {"deps": ['jquery']}
        }
    });

    require(['angular', './controllers', 'angular-route', 'jquery', 'bootstrap'],
        function (angular, controllers) {

            angular.module('kitsKlets', ['ngRoute']).
                config(['$routeProvider', function ($routeProvider) {
                    $routeProvider.when('/main', {
                        templateUrl: 'partials/main.html',
                        "controller": controllers.MainCtrl
                    });
                    $routeProvider.otherwise({redirectTo: '/main'});
                }]);//This is a bit of overkill, given that we only have a single controller.

            angular.bootstrap(document, ['kitsKlets']);

        });
}());