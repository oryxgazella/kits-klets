package controllers

import play.api.libs.json.JsValue
import play.api.mvc._
import play.api.Play.current

class Application extends Controller {
  def index = Action(Ok(views.html.main("Kits-klets")))
  def chatSocket = WebSocket.acceptWithActor[JsValue, JsValue] {
    request => out => Chatter.props(out)
  }
}
