package controllers

import akka.actor.{Actor, ActorRef, Props}
import play.Logger
import play.api.libs.json._

import scala.collection.mutable.ArrayBuffer

class Chatter(out: ActorRef) extends Actor {
  override def preStart(): Unit = {
    Chatter.subscribe(out)
  }

  def receive = {
    case rq: JsObject =>
      Logger.debug {
        "Received a request: " + rq.toString()
      }
    Chatter.broadcast(rq)
  }

  override def postStop() {
    Logger.info("Client disconnecting from stream")
    Chatter.unSubscribe(out)
  }
}

object Chatter {
  private val subscribers = ArrayBuffer[ActorRef]()

  def broadcast(rq: JsObject): Unit = {
    subscribers.foreach{
      actor =>
        //In a real world app we'd probably validate what we're broadcasting to everyone.
        actor ! rq
    }
  }

  def subscribe(out: ActorRef) = {
    if (!subscribers.contains(out))
      subscribers += out
  }

  def unSubscribe(subscriber: ActorRef): Unit = {
    val index = subscribers.indexWhere(_ == subscriber)
    if (index > 0) {
      subscribers.remove(index)
      Logger.info("Unsubscribed client from stream")
    }
  }
  def props(out: ActorRef): Props = Props(new Chatter(out))
}
